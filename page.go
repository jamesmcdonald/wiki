package wiki

// Page is a wiki page
type Page struct {
	Title string
	Body  []byte
}
