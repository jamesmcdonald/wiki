package wiki

import (
	"html/template"
	"net/http"
	"regexp"
)

func noescape(b []byte) template.HTML {
	return template.HTML(b)
}

var fm = template.FuncMap{
	"noescape": noescape,
}

var templates = template.Must(template.New("").Funcs(fm).ParseFiles("tmpl/layout.html", "tmpl/edit.html", "tmpl/view.html", "tmpl/list.html"))

var linkMatcher = regexp.MustCompile("\\[{3}[a-zA-Z]([a-zA-Z0-9 _]?)+\\]{3}")

func linkReplace(in []byte) []byte {
	link := string(in[3 : len(in)-3])
	return []byte("<a href=\"/view/" + link + "\">" + link + "</a>")
}

type linkFixer struct {
	w http.ResponseWriter
}

func (lf linkFixer) Write(p []byte) (int, error) {
	return lf.w.Write(linkMatcher.ReplaceAllFunc(p, linkReplace))
}

func (lf linkFixer) Header() http.Header {
	return lf.w.Header()
}

func (lf linkFixer) WriteHeader(statusCode int) {
	lf.w.WriteHeader(statusCode)
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
