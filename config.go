package wiki

import (
	"fmt"
	"io/ioutil"
	"log"

	toml "github.com/pelletier/go-toml"
)

// Config reads in the configuration file
func readConfig(configfile string) (string, map[string]interface{}, error) {
	rawconf, err := ioutil.ReadFile(configfile)
	if err != nil {
		log.Fatal(err)
	}
	config, err := toml.Load(string(rawconf))
	if err != nil {
		log.Fatal(err)
	}
	if !config.Has("backend") {
		return "", nil, fmt.Errorf("No backend set")
	}
	backend := config.Get("backend").(string)
	if !config.Has(backend) {
		return "", nil, fmt.Errorf("No config section %s found", backend)
	}
	block := config.Get(backend).(*toml.Tree)
	return backend, block.ToMap(), nil
}
