package file

import (
	"gitlab.com/jamesmcdonald/wiki"
	"io/ioutil"
	"log"
	"os"
)

type fileconfig struct {
	path string
}

type FileBackend struct {
	Config fileconfig
}

func makeconfig(path string) fileconfig {
	return fileconfig{path}
}

func (f FileBackend) Save(p *wiki.Page) error {
	filename := f.Config.path + "/" + p.Title + ".md"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func (f FileBackend) Delete(p *wiki.Page) error {
	filename := f.Config.path + "/" + p.Title + ".md"
	return os.Remove(filename)
}

func (f FileBackend) List() ([]string, error) {
	path := f.Config.path
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}
	var pages []string
	for _, file := range files {
		name := file.Name()
		pages = append(pages, name[:len(name)-3])
	}

	return pages, nil
}

func (f FileBackend) Load(title string) (*wiki.Page, error) {
	filename := f.Config.path + "/" + title + ".md"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	return &wiki.Page{Title: title, Body: body}, nil
}

func New(config map[string]interface{}) wiki.Backend {
	cf := makeconfig(config["path"].(string))
	if _, err := os.Stat(cf.path); os.IsNotExist(err) {
		err := os.Mkdir(cf.path, 0700)
		if err != nil {
			log.Fatal(err)
		}
	}
	return FileBackend{Config: cf}
}

func init() {
	wiki.RegisterBackend("file", New)
}
