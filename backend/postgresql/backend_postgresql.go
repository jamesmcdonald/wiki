package postgresql

import (
	"database/sql"
	// Postgres driver
	_ "github.com/lib/pq"
	"gitlab.com/jamesmcdonald/wiki"
	"log"
	"reflect"
)

type pgconfig struct {
	db      *sql.DB
	connstr string
}

type PGBackend struct {
	Config interface{}
}

func makeconfig(connstring string) *pgconfig {
	return &pgconfig{nil, connstring}
}

func getpgconfig(config interface{}) *pgconfig {
	return reflect.ValueOf(config).Interface().(*pgconfig)
}

func getDB(config interface{}) *sql.DB {
	return getpgconfig(config).db
}

func (pg PGBackend) Save(p *wiki.Page) error {
	db := getDB(pg.Config)
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()
	_, err = tx.Exec("insert into page (title, body) values ($1, $2) on conflict(title) do update set body = $2", p.Title, p.Body)
	if err != nil {
		return err
	}
	tx.Commit()
	return err
}

func (pg PGBackend) Delete(p *wiki.Page) error {
	db := getDB(pg.Config)
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()
	_, err = tx.Exec("delete from page where title = $1", p.Title)
	if err != nil {
		return err
	}
	tx.Commit()
	return err
}

func (pg PGBackend) List() ([]string, error) {
	db := getDB(pg.Config)
	var pages []string
	tx, err := db.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()
	rows, err := tx.Query("select title from page order by title")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var title string
		err := rows.Scan(&title)
		if err != nil {
			return nil, err
		}
		pages = append(pages, title)
	}

	return pages, nil
}

func (pg PGBackend) Load(title string) (*wiki.Page, error) {
	db := getDB(pg.Config)
	tx, err := db.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()
	var body []byte
	err = tx.QueryRow("select body from page where title = $1", title).Scan(&body)
	if err != nil {
		return nil, err
	}

	return &wiki.Page{Title: title, Body: body}, nil
}

func New(config map[string]interface{}) wiki.Backend {
	cf := makeconfig(config["connstr"].(string))
	db, err := sql.Open("postgres", cf.connstr)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec("create table if not exists page (id serial primary key, title text not null unique, body text not null)")
	if err != nil {
		log.Fatal(err)
	}
	cf.db = db
	return PGBackend{Config: cf}
}

func init() {
	wiki.RegisterBackend("postgresql", New)
}
