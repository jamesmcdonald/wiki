package wiki

import "errors"

type Backend interface {
	Load(string) (*Page, error)
	List() ([]string, error)
	Save(*Page) error
	Delete(*Page) error
}

var backends = make(map[string]func(map[string]interface{}) Backend)

// RegisterBackend registers a storage backend
func RegisterBackend(name string, b func(map[string]interface{}) Backend) {
	backends[name] = b
}

// New creates a new wiki backend
func NewBackend(configfile string) (Backend, error) {
	backend, config, err := readConfig(configfile)
	if err != nil {
		return nil, err
	}
	if _, ok := backends[backend]; ok {
		initialiser := backends[backend]
		b := initialiser(config)
		return b, nil
	}
	return nil, errors.New("backend not found")
}
