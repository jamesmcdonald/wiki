FROM golang as build

WORKDIR /go/src/wiki
COPY . .

ENV GO111MODULE=on
RUN go get -d -v ./...
RUN go install -v ./cmd/...

FROM debian

RUN mkdir /data
WORKDIR /data
COPY tmpl /data/tmpl
COPY --from=build /go/bin/wiki /bin

EXPOSE 8080

CMD ["wiki"]

