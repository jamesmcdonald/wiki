package main

import (
	"log"

	"gitlab.com/jamesmcdonald/wiki"
	_ "gitlab.com/jamesmcdonald/wiki/backend/file"
	_ "gitlab.com/jamesmcdonald/wiki/backend/postgresql"
)

func main() {
	const configfile = "config.toml"
	server, err := wiki.New(configfile)
	if err != nil {
		log.Fatal(err)
	}
	log.Fatal(server.Serve())
}
