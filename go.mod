module gitlab.com/jamesmcdonald/wiki

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/lib/pq v1.10.3
	github.com/pelletier/go-toml v1.9.4
	github.com/russross/blackfriday/v2 v2.1.0
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
