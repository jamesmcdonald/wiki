A simple wiki based on https://golang.org/doc/articles/wiki/.

Documents are rendered as Markdown using
https://github.com/russross/blackfriday with the extension that you can create
a local page link with triple brackets `[[[Like This]]]`. You can store pages
in either a PostgreSQL database or as files on the filesystem.

You will need to create a `config.toml` file to configure whichever backend you
prefer. It should look something like:

```
[postgresql]
connstr = "host=xxx user=xxx dbname=xxx password=xxx"

[file]
path = "/home/user/wikipages"
```

You can build and run the wiki with:
```
go get github.com/golang/dep/cmd/dep
dep ensure
go run wiki.go <driver>
```

Where `<driver>` is either `file` or `postgresql`.
