package wiki

import (
	"log"
	"net/http"
	"regexp"

	"github.com/russross/blackfriday/v2"
)

// Server is the webserver
type Server struct {
	Backend Backend
}

var validPath = regexp.MustCompile("^/(delete|edit|save|view)/([a-zA-Z]([a-zA-Z0-9 _]?)+)$")

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r, m[2])
	}
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/view/Home", http.StatusFound)
}

func (s Server) listHandler(w http.ResponseWriter, r *http.Request) {
	pages, err := s.Backend.List()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = templates.ExecuteTemplate(w, "list.html", pages)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (s Server) viewHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := s.Backend.Load(title)
	if err != nil {
		http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}
	l := linkFixer{w: w}
	p.Body = blackfriday.Run(p.Body)
	renderTemplate(l, "view", p)
}

func (s Server) editHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := s.Backend.Load(title)
	if err != nil {
		p = &Page{Title: title}
	}
	renderTemplate(w, "edit", p)
}

func (s Server) saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")
	re := regexp.MustCompile(`\r`)
	body = re.ReplaceAllString(body, "")
	p := &Page{Title: title, Body: []byte(body)}
	err := s.Backend.Save(p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

func (s Server) deleteHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := s.Backend.Load(title)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = s.Backend.Delete(p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/list/", http.StatusFound)
}

// Serve sets up and runs the webserver
func (s Server) Serve() error {
	listen := ":8080"
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/view/", makeHandler(s.viewHandler))
	http.HandleFunc("/edit/", makeHandler(s.editHandler))
	http.HandleFunc("/save/", makeHandler(s.saveHandler))
	http.HandleFunc("/delete/", makeHandler(s.deleteHandler))
	http.HandleFunc("/list/", s.listHandler)
	log.Printf("Listening on %s", listen)
	return http.ListenAndServe(listen, nil)
}

// New creates a new wiki server
func New(configfile string) (Server, error) {
	backend, err := NewBackend(configfile)
	if err != nil {
		return Server{}, err
	}
	return Server{Backend: backend}, nil
}
